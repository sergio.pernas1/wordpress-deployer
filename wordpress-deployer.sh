#!/bin/bash
set -e

if test -f /deployed; then

	exit
fi


# instalacion del stack LAMP

echo "Instalando paquetes..."

apt update && apt install apache2 php mariadb-server libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y

echo "listo!"
# creacion de base de datos

echo "Configurando bases de datos..."

mysql -e "CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"

# Creacion de usuario

mysql -e "CREATE USER 'wordpressuser'@'%' IDENTIFIED BY 'password';"

# PRivilegios

mysql -e "GRANT ALL ON wordpress.* TO 'wordpressuser'@'%';"


mysql -e "FLUSH PRIVILEGES;"

echo "listo!"
# wordpress

echo "Desplegando wordpress..."
rm -r /var/www/html

cd /var/www && curl -O https://wordpress.org/latest.tar.gz && tar xf latest.tar.gz && mv wordpress html

cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
echo "listo!"

echo "Configurando bases de datos..."
sed -i 's/database_name_here/wordpress/' /var/www/html/wp-config.php
sed -i 's/username_here/wordpressuser/' /var/www/html/wp-config.php
sed -i 's/password_here/password/' /var/www/html/wp-config.php
echo "listo!"


# Evitar que el script se vuelva a ejecutar
echo "generando fichero /deployed..."
touch /deployed
echo "listo!"
